package a12development.dutycalendarttdf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    DatePicker datePicker;
    Button btn_getDutySchedule;
    Spinner spinner_dutyType;
    TextView tv_instructions;
    String dutyType;
    int lastDutyDay, lastDutyMonth, lastDutyYear;
    int spinnerSelectCount = 0;
    boolean dutyCollected = false;
    public static final String PREFS_NAME = "MyPrefsFile";
    static final String EXTRA_DUTY_TYPE = "a12development.dutycalendarttdf.EXTRA_DUTY_TYPE";
    static final String EXTRA_LAST_DUTY_DAY = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_DAY";
    static final String EXTRA_LAST_DUTY_MONTH = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_MONTH";
    static final String EXTRA_LAST_DUTY_YEAR = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_YEAR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean dutyStored = settings.getBoolean("dutyStored",false);
        if(dutyStored) {
            getDutySchedule(dutyStored);
        }

        datePicker = (DatePicker) findViewById(R.id.datePicker);
        btn_getDutySchedule = (Button) findViewById(R.id.btn_getDutyDays);
        spinner_dutyType = (Spinner) findViewById(R.id.spinner_dutyType);
        tv_instructions = (TextView) findViewById(R.id.tv_instructions);


        //Attach user choices to spinner_dutyType
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.dutyType_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_dutyType.setAdapter(adapter);

        spinner_dutyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // spinner.setOnItemSelectedListener(this); Will call `onItemSelected` Listener.
                spinnerSelectCount++;
                if(spinnerSelectCount>1) {
                    tv_instructions.setVisibility(View.VISIBLE);
                    /*set the min date of the datepicker to the start of the current month*/
                    setDatePickerMinDate(datePicker);
                    datePicker.setVisibility(View.VISIBLE);

                    dutyType = parent.getItemAtPosition(position).toString();
                    Log.i(dutyType, "dutyType");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Attach OnDateChangedListener to datePicker
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                btn_getDutySchedule.setVisibility(View.VISIBLE);
                lastDutyDay = dayOfMonth;
                lastDutyMonth = monthOfYear;
                lastDutyYear = year;
                dutyCollected=true;
            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop(){
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        if(dutyCollected) {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

            editor.putBoolean("dutyStored", true);
            editor.putInt("firstDutyDay", lastDutyDay);
            editor.putInt("firstDutyMonth", lastDutyMonth);
            ;
            editor.putInt("firstDutyYear", lastDutyYear);
            editor.putString("dutyType", dutyType);
            // Commit the edits!
            editor.commit();
        }
    }



    public void getDutySchedule(View v) {

        Log.i("getDutySchedule","firstpick");
       int dayOfMonth = datePicker.getDayOfMonth();
       Toast.makeText(this, dayOfMonth+"", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DutyScheduleActivity.class);
        intent.putExtra(EXTRA_DUTY_TYPE,dutyType);
        intent.putExtra(EXTRA_LAST_DUTY_DAY, lastDutyDay);
        intent.putExtra(EXTRA_LAST_DUTY_MONTH, lastDutyMonth);
        intent.putExtra(EXTRA_LAST_DUTY_YEAR, lastDutyYear);
        startActivity(intent);
    }

    /*this method is called to start the dutyScheduleActivity if the information was retrieved from
    * the SharedPreferences file*/
    public void getDutySchedule(boolean dutyStored) {
        Log.i("getDutySchedule","sharedPrefs");
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        Intent intent = new Intent(this, DutyScheduleActivity.class);
        intent.putExtra(EXTRA_DUTY_TYPE, settings.getString("dutyType", "1 in 4"));
        intent.putExtra(EXTRA_LAST_DUTY_DAY, settings.getInt("firstDutyDay", 1));
        intent.putExtra(EXTRA_LAST_DUTY_MONTH, settings.getInt("firstDutyMonth",1));
        intent.putExtra(EXTRA_LAST_DUTY_YEAR, settings.getInt("firstDutyYear",2016));
        startActivity(intent);
    }

    public void setDatePickerMinDate(DatePicker dp){

                    /*set the min date of the datepicker to the start of the current month*/
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        c.set(year, month, 1);
        dp.setMinDate(c.getTimeInMillis());
    }
}


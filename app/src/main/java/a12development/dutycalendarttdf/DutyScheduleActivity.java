package a12development.dutycalendarttdf;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;


public class DutyScheduleActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "MyPrefsFile";
    static final String EXTRA_DUTY_TYPE = "a12development.dutycalendarttdf.EXTRA_DUTY_TYPE";
    static final String EXTRA_LAST_DUTY_DAY = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_DAY";
    static final String EXTRA_LAST_DUTY_MONTH = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_MONTH";
    static final String EXTRA_LAST_DUTY_YEAR = "a12development.dutycalendarttdf.EXTRA_LAST_DUTY_YEAR";

    String dutyType;
    int firstDutyDay;//stores the first duty day entered for the this calendar app,tobe stored in the preference file
    int firstDutyDayInMonth;//storesthefirst duty day in the most current month shown/navigated to
    int firstDutyMonth, dutyMonth, firstDutyYear;//stores the first duty month and year entered with this calendar app
    int lastDutyDay, lastDutyMonth, lastDutyYear, nextDutyDay;
    int monthChangeCount = 0;
    boolean dutyStored = true;

    CaldroidFragment caldroidFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duty_schedule);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        //Restore preferences

        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, false); //disable swipe
        caldroidFragment.setArguments(args);

        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        Intent intent = getIntent(); //get the intent that started the activity
        dutyType = intent.getStringExtra(EXTRA_DUTY_TYPE);
        lastDutyDay = intent.getIntExtra(EXTRA_LAST_DUTY_DAY, 0);
        firstDutyDay = firstDutyDayInMonth = lastDutyDay;
        firstDutyMonth = dutyMonth = lastDutyMonth = intent.getIntExtra(EXTRA_LAST_DUTY_MONTH, 0);
        firstDutyYear = lastDutyYear = intent.getIntExtra(EXTRA_LAST_DUTY_YEAR, 0);

        showDutySchedule(lastDutyDay, lastDutyMonth, lastDutyYear, dutyType);

        caldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onCaldroidViewCreated() {
                Log.i("caldroid", "view created");
                Button leftButton = caldroidFragment.getLeftArrowButton();
                //Button rightButton = caldroidFragment.getRightArrowButton();
                //TextView textView = caldroidFragment.getMonthTitleTextView();

                if(firstDutyMonth==dutyMonth && lastDutyYear==firstDutyYear){
                    leftButton.setVisibility(View.INVISIBLE);
                }
                else{
                    leftButton.setVisibility(View.VISIBLE);
                }

                // Do customization here
            }

            @Override
            public void onSelectDate(Date date, View view) {

            }

            @Override
            public void onChangeMonth(int month, int year) {
                monthChangeCount+=1;

                Button leftButton = caldroidFragment.getLeftArrowButton();

                if(month-1==firstDutyMonth && year==firstDutyYear){
                    leftButton.setVisibility(View.INVISIBLE);
                }
                else{
                    leftButton.setVisibility(View.VISIBLE);
                }

               //if the dutySchedule was not calculated for this month if not already created
                /*
                *conditions to calculate the duty schedule for another month
                * condition 1: same year of first duty date (firstDutyYear==year && lastDutyYear==year), a new month further from duty date (month-1>lastDutyMonth)
                * condition 2: a new month further from last duty date (month-1>lastDutyMonth) and it's not the same year as the firstDutyYear(firstDutyYear!=year)
                * condition 3: first month of a new year (month==1), lastDutyYear was in the previous year (lastDutyYear==year && firstDutyYear==year)
                 */
               if((monthChangeCount>1 && month-1>firstDutyMonth && month-1>lastDutyMonth && firstDutyYear==year && lastDutyYear==year)
                       || (monthChangeCount>1 && month-1>lastDutyMonth && firstDutyYear!=year)
                       || (monthChangeCount>1 && month==1 && lastDutyYear!=year && firstDutyYear!=year)){

                   showDutySchedule(lastDutyDay,month,year,dutyType);//showDutySchedule(lastDutyDay,lastDutyMonth,lastDutyYear,month,dutyType);

               }
            }

        });


    }
    @Override
    protected void onStop(){
        super.onStop();

        // We need an Editor object to make preference changes.
        // All objects are from android.context.Context
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("dutyStored", dutyStored);
        editor.putInt("firstDutyDay", firstDutyDay);
        editor.putInt("firstDutyMonth", firstDutyMonth);;
        editor.putInt("firstDutyYear", firstDutyYear);
        editor.putString("dutyType", dutyType);
        // Commit the edits!
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        //prevent any action
        return;
    }

    public boolean isLeapYear(int year){
        if(year%4==0) return true;
        return false;
    }

    /**
     * @Assumes the month uses the Calendar month which starts at 0
     * @param month
     * @param year
     * @return days in month of that year
     */
    public int daysInMonth(int month,int year){
        //30 days in september, april, june and november, all the rest have 31 except february alone.
        int days = 31;
        //recall the calendar starts from 0, so february is 1

        switch(month){
            case 1: //february
                if(isLeapYear(year)) days=29;
                else days=28;
                break;
            case 3: days=30; //april
                break;
            case 5: days=30; //june
                break;
            case 8: days=30; //september
                break;
            case 10: days=30; //november
                break;
            default: days=31;
                break;
        }
        return days;
    }

    public int generateNextDutyDay(int dd, int dm, int dy, String dt){

        switch(dt){
            case "1 in 3": //do something
                return dd+3;

            case "1 in 4":
            case "1 in 3 with day off":
                return dd+4;

            case "1 in 4 with day off":
                return dd+5;

            default: return -1;

        }
    }


    /*This method is executed the first time the user enters their last known duty day
    * it colours the duty days onwards for the rest of the month and previous duty days till the start of the month
    * the last duty day in that month is stored in the variable 'nextDutyDay'
    * the first duty day in that month is stored in the variable 'firstDutyDay'
    *
    * the onChangeMonth() method determines when this method is called*/
    public void showDutySchedule(int dd, int dm, int dy, String dt){
        //colour last duty day grey
        Calendar cal2 = Calendar.getInstance();
        if(dm==lastDutyMonth) {
            //colour the current duty day grey because it's the first duty date entered
           cal2.set(dy, dm, dd);
           caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_lighter_gray, cal2.getTime());


           //fill out duty dates for current month
           //assumes lastDutyDay was this current month
           nextDutyDay = generateNextDutyDay(dd, dm, dy, dt);
           //generate next duty days
            int count = 0;
           while (nextDutyDay <= daysInMonth(dm, dy)) {
               count++;
               cal2 = Calendar.getInstance();
               cal2.set(dy, dm, nextDutyDay);
               caldroidFragment.setBackgroundResourceForDate(R.color.colorPrimary, cal2.getTime());
               dd = lastDutyDay = nextDutyDay;
               nextDutyDay = generateNextDutyDay(dd, dm, dy, dutyType);
           }
            if(count<=1){
                /*the next duty day is in another month but it should still be coloured*/
                cal2.set(dy, dm, nextDutyDay);
                caldroidFragment.setBackgroundResourceForDate(R.color.colorPrimary, cal2.getTime());
            }
       }
        if(dm>lastDutyMonth || (dm==1 && dy!=firstDutyYear)){
            Log.i("forward lastDutyDay",lastDutyDay+"");
            dm=dm-1;
            int daysInLastMonth = daysInMonth(lastDutyMonth,dy);
            //generate nextDutyDay based on LastMonth's duty day so
            nextDutyDay = generateNextDutyDay(lastDutyDay, lastDutyMonth, lastDutyYear, dutyType);
            //because we are calculating with lastMonth it will exceed the number of days in the
            //last month, which takes us to the new month
            if(nextDutyDay > daysInLastMonth){
                Log.i("forward daysInLastMonth",daysInLastMonth+"");
                nextDutyDay = nextDutyDay - daysInLastMonth;
            }
            //set the calendar to this new duty day for this duty month and this duty year
            cal2.set(dy, dm, nextDutyDay);
            caldroidFragment.setBackgroundResourceForDate(R.color.colorPrimary, cal2.getTime());
            //the first dutyday of this month
            firstDutyDayInMonth = nextDutyDay;
            //update the values of lastDutyMonth and lastDutyYear
            lastDutyMonth = dm;
            lastDutyYear = dy;
            int daysInMonth = daysInMonth(dm,dy);
            //generate the rest of the duty days for the month
            int count = 0;
            while (nextDutyDay <= daysInMonth) {
                count++;
                //setting the lastDutyDay of the current month because calculating the new one so that it stays valid
                lastDutyDay = nextDutyDay;
                nextDutyDay = generateNextDutyDay(lastDutyDay, dm, dy, dutyType);
                cal2.set(dy, dm, nextDutyDay);
                caldroidFragment.setBackgroundResourceForDate(R.color.colorPrimary, cal2.getTime());
                Log.i("nextDutyDay",nextDutyDay+"");
            }

        }


    }

    public void getNewDutySchedule(View view){
        dutyStored = false;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("dutyStored", dutyStored);
        // Commit the edits!
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }







}
